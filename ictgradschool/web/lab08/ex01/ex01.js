"use strict";

// TODO Declare variables here
var myFirstNumber = 3;
var myFirstBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello";
var mySecondString = "World";

var x = myFirstNumber + 7;
var a = mySecondNumber - myFirstNumber;
var HelloWorld = myFirstString + " " + mySecondString;
var b = myFirstString + myFirstNumber;
var c = mySecondString - mySecondNumber;

// TODO Use console.log statements to print the values of the variables to the command line.
console.log(x);
console.log(a);
console.log(HelloWorld);
console.log(b);
console.log(c);